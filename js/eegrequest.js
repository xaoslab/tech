var html;
var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    var jsonObj = JSON.parse(this.responseText);
    //document.getElementById("demo").innerHTML = jsonObj.name;
    for(i=0; i<jsonObj.length; i++) {
      var style = 'emuse_bg_1';
      var num   = i + 1;
      if (i % 2 == 1) { style = 'emuse_bg_2'; }
      html += '<div class="emuse_file ' + style + '"><div class="emuse_fname"><span class="emuse_fnum">' + num +'.</span>&nbsp;' + jsonObj[i].hminm + '&nbsp;<span class="emuse_fdate">uploaded on ' + jsonObj[i].udate + '</span></div><div class="emuse_fplot"><a href="' + 'http://eeg.xaoslab.tech/xaoslabplot_' + jsonObj[i].recid + '.html' + '"><i class="fa fa-area-chart"></i></a></div></div>'
    }
    if (typeof html !== 'undefined') {
      document.getElementById("filedata").innerHTML = html;
    }
  }
};
xmlhttp.open("GET", "http://eeg.xaoslab.tech/xaoslabjson.json", true);
xmlhttp.send();
