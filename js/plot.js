class ChartGrid {
    constructor(rootEl) {
      this.rootEl = rootEl;
      this.currScreen = $(window).width();//screen.width;
      this.margin = {
          top: 20,
          right: 20,
          bottom: 20,
          left: 20};
      this.width = /*this.rootEl.attr("width")*/
                  ( this.currScreen > 1920 ? this.currScreen : 1920 ) - this.margin.left - this.margin.right;
      this.height = this.rootEl.attr("height")
                   - this.margin.top - this.margin.bottom;
	  $('#waveform').attr('width', ( this.currScreen > 1920 ? this.width : 1920 ) );
	  $('.div-waveform-bg').css('width', ( this.currScreen > 1920 ? this.width : 1920 ) );
    }

    setupScale(x, y) {
      this.xScale = d3.scaleLinear()
        .domain(x)
        .range([this.margin.left, this.width+this.margin.left]);

      this.yScale = d3.scaleLinear()
        .domain(y)
        .range([this.margin.top, this.height+this.margin.top]);

      this.xAxis = d3.axisBottom(this.xScale)
          .tickSize(this.height);

      this.yAxis = d3.axisLeft(this.yScale)
          .tickSize(this.width);

      return this;
    }

    setupGrid() {
      this.g = this.rootEl.append("g");

      this.gridH = this.g.append("g")
          .attr("class", "x axis")
          .attr("transform",
                "translate(" + 0 + "," + this.margin.top + ")");

      this.gridV = this.g.append("g")
          .attr("class", "y axis")
          .attr("transform",
                "translate(" + (this.margin.left+this.width) + "," + 0 + ")");

      this.gridH.call(this.xAxis);
      this.gridV.call(this.yAxis);

      this.gridV.select(".domain").remove();
      this.gridH.select(".domain").remove();

      this.gridH.selectAll(".x.axis text")
        .attr('y',this.yScale(0)-this.margin.top)
        .attr('text-anchor', 'end')
        .attr("transform", "translate(-3,5)")
        .attr('fill', undefined);

      this.gridV.selectAll(".y.axis text")
        .attr('x',this.xScale(0)-(this.margin.left+this.width))
        .attr("transform", "translate(-2,8)")
        .attr('fill', undefined);

      return this;
    }

    curvedLine(data,color,opacity,cssclass) {
      var line = d3.line()
        .curve(d3.curveCardinal)
        .x((d) => this.xScale(d.x))
        .y((d) => this.yScale(d.y));

      d3.selectAll("."+cssclass).remove();
      var path = this.rootEl.append("path");

      path.data([data])
          .attr("class", cssclass)
          .attr("d", line)
          .attr("stroke", color)
          .attr("stroke-width", 2)
          .attr("fill", "none")
	  .attr("stroke-opacity", opacity);

      return path;
    }

    point(coords) {
      this.rootEl
        .append("circle")
        .attr("cx",this.xScale(coords.x))
        .attr("cy",this.yScale(coords.y))
        .attr("r","3")
        .attr("fill", "red");

    }
}

const sampleRate = 22000;
const length = 1000;
let numOfSamples = Math.floor(sampleRate*length/1000)+1;
const chart = new ChartGrid(d3.select("#waveform"));
chart.setupScale([0, 100], [1, -1]);
chart.setupGrid();

const data = Array.from(
      {length: numOfSamples},
      (v, k) => {
        var _y = Math.cos(k/88);
        //if (_y < -0.999 ) { console.log(k) }
        if ( k > 273 && k < 1932 ){ _y = -0.999; }
        return {
          x: k/22,
          y: _y
        };
      }
);

const data_2 = Array.from(
      {length: numOfSamples},
      (v, k) => {
        var _y = ( k <= 312 ) ? ( Math.sin(k/50 - 3.14/2) ) : -Math.sin(k/22.35); //-Math.sin(k/88);
        //if (_y < -0.999 ) { console.log(k) }
        if ( ( k > 312 && k < 1579 ) || ( k >= 2141) ){ _y = -0.999; }
        return {
          x: k/22,
          y: _y
        };
      }
);

const data_3 = Array.from(
      {length: numOfSamples},
      (v, k) => {
        var _y = Math.sin(k/22.35);
        //if (_y < -0.999 ) { console.log(k) }
        if ( ( k < 1509 ) || ( k >= 2071) ){ _y = -0.999; }
        return {
          x: k/22,
          y: _y
        };
      }
);

const data_4 = Array.from(
      {length: numOfSamples},
      (v, k) => {
        var _y = ( k < 1450 ) ? Math.sin(k/40) : -Math.sin(k/5.6 - 2.5);
        //if (_y < -0.99 ) { console.log(k) }
        if ( ( k < 183) || ( k > 1440 && k < 1571 ) || ( k >= 2064 ) ){ _y = -0.999; }
        return {
          x: k/22,
          y: _y
        };
      }
)

const data_5 = Array.from(
      {length: numOfSamples},
      (v, k) => {
        var _y = ( k < 1588 ) ? -Math.sin(k/40) : Math.sin(k/5.6 - 2.5);
        //if (_y < -0.999 ) { console.log(k) }
        if ( ( k < 313 ) || ( k >= 2081) ){ _y = -0.999; }
        return {
          x: k/22,
          y: _y
        };
      }
);

var d_filtered_1 = data.filter((el) => el.x<=100);
var d_filtered_2 = data_2.filter((el) => el.x<=100);
var d_filtered_3 = data_3.filter((el) => el.x<=100);
var d_filtered_4 = data_4.filter((el) => el.x<=100);
var d_filtered_5 = data_5.filter((el) => el.x<=100);

chart.curvedLine(d_filtered_1,"#c200ff",0.35,'data_1');
chart.curvedLine(d_filtered_2,'lime',0.35,'data_2');
chart.curvedLine(d_filtered_3,'white',0.35,'data_3');
chart.curvedLine(d_filtered_4,'red',0.35,'data_4');
chart.curvedLine(d_filtered_5,'blue',0.35,'data_5');
