var polys    = 151;
var koeff_b  = 4;
var koeff_a  = 8;
var koeff_t1 = 8;
var koeff_t2 = 8;
var max_s    = 25;
var r_prev   = 0;
var delta    = 10;
var zmeika_stop = 0;
var opdevider   = 3;
var ZmeikaTimer;

var animtn_a_cntr  = 0;
var animtn_b_cntr  = 0;
var animtn_t1_cntr = 0;
var animtn_t2_cntr = 0;
var animtn_d_cntr  = 0;

var beta_idle_sec_lim  = 3;
var alpha_idle_sec_lim = 3;
var t1_idle_sec_lim = 1;
var t2_idle_sec_lim = 1;

var prev_step   = 0.005;
var prev_alpha  = 0;
var prev_beta   = 0;
var prev_htheta = 0;
var prev_ltheta = 0;
var prev_delta  = 0;

function randomInt(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1);
    rand = Math.round(rand);
    return rand;
}

function randomInt2(min, max) {
    /*var rand = min - 0.5 + Math.random() * (max - min + 1);
    while( Math.abs(rand - r_prev) < delta ) {
        rand = min - 0.5 + Math.random() * (max - min + 1)
    }
    rand = Math.round(rand);
    r_prev = rand;
    return rand;*/
	return 13;
}

function hideAllPolys() {
    for(var i=0; i<=polys; i++) {
        $( "#p" + i ).fadeOut();
    }
}

function hidePolys(wave,to) {
    for(var i=0; i<polygons.length; i++) {
		if( polygons[i].wave == wave ) {
            $( "#" + polygons[i].id).fadeTo(300, to);
		}
    }
}

function polyIn(wave, id, delay, first) {
	if (!($( "#" + id ) === undefined )) {
        $( "#" + id ).delay(delay).fadeTo(300, $( "#polygons").attr(wave+"-opacity"));
		if (first) {
			//console.log(id + ":" + first + "," + delay);
			$( "#polygons").delay(delay).attr(wave,"1");
		}
    }
}

function polyOut(wave, id, delay, to, last) {
	var koeff;
	if (wave == "alpha") {
		koeff = koeff_a;
	} else if (wave == "beta") {
		koeff = koeff_b;
	} else if (wave == "ltheta") {
		koeff = koeff_t2;
	} else if (wave == "htheta") {
		koeff = koeff_t1;
	}
    if (!($( "#" + id ) === undefined )) {
        $( "#" + id ).delay(delay).fadeTo(300, to);
		if (last) {
			var finalize_true = setTimeout(
				function() {
					$( "#polygons").delay(delay).attr(wave,"0");
					setOpacity(wave, $( "#polygons").attr(wave+"-opacity"));
					//console.log(id + ":" + last + "," + delay + ". set " + wave + " opacity " + getOpacityVal(wave));
				},
			delay*koeff
			);
		}
    }
}

function _zmeika(delay,reverse) {
    var data = new Array;
    fsync = false;
    var poly_opacity = Math.random().toFixed(1);
    poly_opacity     = poly_opacity < 0.2 ? 0.2 : poly_opacity;
    $( ".polypath").css("fill-opacity", poly_opacity==0?0.1:poly_opacity)
    if (!reverse) {
        for(var i=0; i<=polys; i++) {
            data.push({in_id: i+1, in_delay: (i+1)*delay, out_id: i, out_delay: i*delay });
	        polyOut( data[i]["out_id"], data[i]["out_delay"]);
	        polyIn( data[i]["in_id"], data[i]["in_delay"]);
        }
    } else {
        var j = 0;
        for(var i=(polys+1); i>0; i--) {
            data.push({in_id: i-1, in_delay: (j+1)*delay, out_id: i, out_delay: j*delay });
	        polyOut( data[j]["out_id"], data[j]["out_delay"]);
	        polyIn( data[j]["in_id"], data[j]["in_delay"]);
            j++;
        }
    }
    if (zmeika_stop == 0) {
    ZmeikaTimer = setTimeout(
        function() {
            fsync = true;
            //console.log("zmeika() with params [delay=" + delay + ", reverse="+ (reverse==0?false:true) + ",  opacity=" + poly_opacity + "] is ended");
            zmeika(randomInt2(delta,max_s), randomInt(0,1)); },
        koeff*polys*delay
    );
    } else { /*console.log( "zmeika() is stopped"); */}
}

function getWavePolys(wave) {
    var polys = new Array;
    for(var i=0; i<polygons.length; i++) {
		if( polygons[i].wave == wave ) {
			polys.push(polygons[i].id);
		}
	}
	return polys;
}

function zmeika(wave,delay,reverse) {
    var data   = new Array;
	var wpolys = getWavePolys(wave);
	var op = $( "#polygons").attr(wave+"-opacity")/opdevider;
    if (!reverse) {
        for(var i=0; i<=wpolys.length; i++) {
            data.push({in_id: wpolys[i], in_delay: (i+1)*delay, out_id: wpolys[i-1], out_delay: i*delay });
	        polyOut(wave, data[i]["out_id"], data[i]["out_delay"], op, (i == wpolys.length ? true : false ) );
	        polyIn(wave, data[i]["in_id"], data[i]["in_delay"], (i == 0 ? true : false ) );
        }
    } else {
        var j = 0;
        for(var i=(wpolys.length+1); i>=0; i--) {
            data.push({in_id: wpolys[i-1], in_delay: (j+1)*delay, out_id: wpolys[i], out_delay: j*delay });
	        polyOut(wave, data[j]["out_id"], data[j]["out_delay"], op, (i == 0 ? true : false ) );
	        polyIn(wave, data[j]["in_id"], data[j]["in_delay"], (i == (wpolys.length) ? true : false ) );
            j++;
        }
    }
}

function startZmka(wave) {
	zmeika_stop = 0;
	hidePolys(wave, $( "#polygons").attr(wave+"-opacity")/opdevider);
    zmeika(wave, randomInt2(delta,max_s), randomInt(0,1));
}

function stopZmka() {
    zmeika_stop = 1;
}

function drawPolys(){
	var svgNS = "http://www.w3.org/2000/svg";
	var koeff = 1;
	var eta_w = 594;
	var eta_h = 688;
	var svg_h = parseInt($( ".svg-canvas" ).css("height"));
	var svg_w = parseInt($( ".svg-canvas" ).css("width"));
	if (svg_w < eta_w) {
		koeff = (svg_w/eta_w);
	}
	//console.log(svg_w + "," + svg_h);
	for(var i=0; i<polygons.length; i++) {
		var path = document.createElementNS(svgNS,"path");
		var d = '';
		for(var j=0; j<polygons[i].data.length; j++) {
			d = d + polygons[i].data[j].t + polygons[i].data[j].x*koeff + ',' + polygons[i].data[j].y*koeff + ' ';
		}
		path.setAttributeNS(null,"id", polygons[i].id);
		path.setAttributeNS(null,"class",polygons[i].class);
		path.setAttributeNS(null,"filter","url(#f1)");
		path.setAttributeNS(null,"d", ( d + ' Z' ));
		document.getElementById("polygons").appendChild(path);
	}
}

function resetOpacity() {
	hideAllPolys();
	finalize = setTimeout(
        function() {
            prev_alpha  = 0;
			prev_beta   = 0;
			prev_htheta = 0;
			prev_ltheta = 0;
			prev_delta  = 0;
			setOpacity("alpha", 0);
			setOpacity("beta", 0);
			setOpacity("htheta", 0);
			setOpacity("ltheta", 0);
			setOpacity("delta", 0);
			$( "#polygons").attr("alpha","0");
			$( "#polygons").attr("beta","0");
			$( "#polygons").attr("ltheta","0");
			$( "#polygons").attr("htheta","0");
		},
        1000
    );
}

function animateOpacity(wave, polygon_id, opacity) {
	var delay   = 200;
	var a_stp   = 0.2;
	var b_stp   = 0.5;
	var t_stp   = 0.1;
	var d_stp   = 0.2;
	var w_stp   = 0;
	var animate = true;
	var prev    = 0;
	var trend   = false;
	if (wave == "alpha") {
		prev  = prev_alpha;
		w_stp = a_stp;
	} else if (wave == "beta") {
		prev  = prev_beta;
		w_stp = b_stp;
	} else if (wave == "htheta") {
		prev  = prev_htheta;
		w_stp = t_stp;
	} else if (wave == "ltheta") {
		prev  = prev_ltheta;
		w_stp = t_stp;
	} else if (wave == "delta") {
		prev  = prev_delta;
		w_stp = d_stp;
	}
	if ( (prev > 0) && (opacity > 0) && animate ) {
		if ( ( opacity - prev > 0 ) ) {
			trend == true;
			var prognoz   = ( parseFloat(opacity) + w_stp ).toFixed(2);
			var to_v_true = ( prognoz <= 1 ? prognoz : 1 );
			var finalize_true = setTimeout(
				function() {
					//console.log(wave+"{true trend}: " + opacity + "->" + to_v_true );
					$( "#" + polygon_id).fadeTo("slow", to_v_true);
				    $( "#" + polygon_id).delay(delay).fadeTo("slow", opacity);
				},
			delay
			);
		} else {
			trend == false;
			var prognoz   = ( parseFloat(opacity) - w_stp ).toFixed(2);
			var to_v_false = ( prognoz <= 1 ? prognoz : 1 );			
			var finalize_false = setTimeout(
				function() {
				    $( "#" + polygon_id).fadeTo("slow", to_v_false);
				    $( "#" + polygon_id).delay(delay).fadeTo("slow", opacity);
				},
			delay
			);
		}
	} else {
		if ( prev != opacity ) {
			$( "#" + polygon_id).css("opacity", ( opacity >= 0 && opacity <=1 ) ? opacity : 0 );
			console.log(wave+":" +prev + ', '+opacity+', '+animate);
		}
	}
}

function setOpacity(wave, opacity){
	var _wave = wave;
	$( "#polygons").attr(wave+"-opacity", opacity);
	for(var i=0; i<polygons.length; i++) {
		if( polygons[i].wave == _wave ) {
			if ( $( "#" + polygons[i].id).css("display") == "none" ) {
				$( "#" + polygons[i].id).css("opacity", ( opacity >= 0 && opacity <=1 ) ? opacity : 0 );
				if (opacity < 0.4) {
					if (opacity == 0) {
						$( "#" + polygons[i].id).css("display", "none");
					} else {
						$( "#" + polygons[i].id).css("display","block");
					}
					//console.log(_wave + ":block");
				} else {
					$( "#" + polygons[i].id).fadeIn();
					//console.log(_wave + ":fadein");
				}
			} else {
				animateOpacity(_wave, polygons[i].id, opacity);
			}
			//$( "#" + polygons[i].id).fadeOut();
			//$( "#" + polygons[i].id).delay(10).fadeIn();
		}
	}
}

function getOpacityVal(eegpower) {
	var value = 0;
	if( (eegpower >= -1) && (eegpower <= 1) ) {
		if ( eegpower < 0 ) {
			value = ( 0.5 + eegpower/2 );
		} else {
			value = ( 0.5 + eegpower/2 );
		}
	}
	return value.toFixed(2);
}

function brainWave(seconds, alpha, beta, htheta, ltheta, delta) {
	var logging = false;
	var opacity = 0;
	var wave;
	if( !(typeof alpha === 'undefined') ) {
		wave    = "alpha";
		opacity = getOpacityVal(alpha);
		var zw  = $( "#polygons").attr(wave);
		if ( Math.abs( opacity - prev_alpha ) >= prev_step ) {
			$( "#polygons").attr(wave+"-opacity",opacity);
			if (zw == 0 || (typeof zw === 'undefined') ) {
			    setOpacity(wave, opacity);
			    prev_alpha = opacity;
			    animtn_a_cntr++;
			}
			//console.log("α: set opacity to " + prev_alpha);
		} else {
			if (animtn_a_cntr > alpha_idle_sec_lim) {
				if (zw == 0 || (typeof zw === 'undefined') ) {
				    startZmka(wave);
				    animtn_a_cntr = 0;
				}
			}
			animtn_a_cntr++;
		}
		if (logging) {
			console.log("α: " + getOpacityVal(alpha) + "/" + prev_alpha + " {" + alpha + "}");
		}
	}
	if ( !(typeof beta === 'undefined') ) {
		wave    = "beta";
		opacity = getOpacityVal(beta);
		var zw  = $( "#polygons").attr(wave);
		if ( Math.abs( opacity - prev_beta ) >= prev_step ) {
			$( "#polygons").attr(wave+"-opacity", opacity);
			if (zw == 0 || (typeof zw === 'undefined') ) {
			    setOpacity(wave, opacity);
			    prev_beta = opacity;
				animtn_b_cntr = 0;
			}
			//console.log("β: set opacity to " + prev_beta);
		} else {
			if (animtn_b_cntr > beta_idle_sec_lim) {
				if (zw == 0 || (typeof zw === 'undefined') ) {
				    startZmka(wave);
				    animtn_b_cntr = 0;
				}
			}
			animtn_b_cntr++;
		}
		if (logging) {
			console.log("β:  " + opacity + "/" + prev_beta + " {" + beta + "}");
		}
	}
	if ( !(typeof htheta === 'undefined') ) {
		wave    = "htheta";
		opacity = getOpacityVal(htheta);
		var zw  = $( "#polygons").attr(wave);
		if ( Math.abs( opacity - prev_htheta ) >= prev_step ) {
			$( "#polygons").attr(wave+"-opacity", opacity);
			if (zw == 0 || (typeof zw === 'undefined') ) {
			    setOpacity(wave, opacity);
			    prev_htheta = opacity;
				animtn_t1_cntr = 0;
			}
			//console.log("θh: set opacity to " + prev_htheta);
		} else {
			if (animtn_t1_cntr > t1_idle_sec_lim) {
				if (zw == 0 || (typeof zw === 'undefined') ) {
				    startZmka(wave);
				    animtn_t1_cntr = 0;
				}
			}
			animtn_t1_cntr++;
		}
		if (logging) {
			console.log("θh:  " + opacity + "/" + prev_htheta + " {" + htheta + "}");
		}
	}
	if ( !(typeof ltheta === 'undefined') ) {
		wave    = "ltheta";
		opacity = getOpacityVal(ltheta);
		var zw  = $( "#polygons").attr(wave);
		if ( Math.abs( opacity - prev_ltheta ) >= prev_step ) {
			$( "#polygons").attr(wave+"-opacity", opacity);
			if (zw == 0 || (typeof zw === 'undefined') ) {
			    setOpacity(wave, opacity);
			    prev_ltheta = opacity;
				animtn_t2_cntr = 0;
			}
			//console.log("θl: set opacity to " + prev_ltheta);
		} else {
			if (animtn_t2_cntr > t2_idle_sec_lim) {
				if (zw == 0 || (typeof zw === 'undefined') ) {
				    startZmka(wave);
				    animtn_t2_cntr = 0;
				}
			}
			animtn_t2_cntr++;
		}
		if (logging) {
			console.log("θl:  " + opacity + "/" + prev_ltheta + " {" + ltheta + "}");
		}
	}
	if ( !(typeof delta === 'undefined') ) {
		opacity = getOpacityVal(delta);
		if ( Math.abs( opacity - prev_delta ) >= prev_step ) {
			setOpacity("delta", opacity);
			prev_delta = opacity;
			animtn_d_cntr++;
			//console.log("δ: set opacity to " + prev_ltheta);
		}
		if (logging) {
			console.log("δ:  " + opacity + "/" + prev_delta + " {" + delta + "}");
		}
	}
}

drawPolys();
hideAllPolys();
//setOpacity("beta",.7);
//zmeika(randomInt2(delta,max_s), randomInt(0,1));
