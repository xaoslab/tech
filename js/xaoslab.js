/* xaoslab.tech generic JS worker
 * developed by konstantin@narkhov.pro
 */

'use strict';
//const URL = '../media/theta_relax.mp3';
//const URL = 'https://assets.ctfassets.net/5hmfzlhpdavr/6CEHhtRvGQu455uTRseqO3/43e28cac3e0aaedcad2ff5cce40d2539/Hospitalized_-_All_Alone___Dubstep___138_BPM___wowa.me.mp3';
const canvasHeight = 400;
const fftSize = 1024
const oscCanvas_l = document.getElementById("oscilloscope_l");
const oscCanvas_r = document.getElementById("oscilloscope_r");
const context = new(window.AudioContext || window.webkitAudioContext)();
const analyser_l = context.createAnalyser();
analyser_l.fftSize = 1024;
analyser_l.minDecibels = -90;
analyser_l.maxDecibels = -10;
analyser_l.smoothingTimeConstant = 0.8;
const analyser_r = context.createAnalyser();
analyser_r.fftSize = 1024;
analyser_r.minDecibels = -90;
analyser_r.maxDecibels = -10;
analyser_r.smoothingTimeConstant = 0.8;
$('.svg-canvas').removeClass('hidden-element');
$('.svg-map').removeClass('hidden-element');
$('#analyzer').removeClass('hidden-element');
$('#analyzer').addClass('hidden-element');
const splitter = context.createChannelSplitter(2);
const merger = context.createChannelMerger(2);
splitter.connect(analyser_l, 0);
splitter.connect(analyser_r, 1);
splitter.connect(merger, 1, 0);
var downloadTimer;
var data_play = [];
let playBuffer;
let source;

$('#body').click(function() {
    //console.log("body click!");
    if ($('#img-sandwich:hover').length == 0 && $('.panel-wrap:hover').length == 0 && $('#clicker').prop('checked')) {
        //console.log("closing panel: " + $( '#clicker' ).prop('checked') + "," + $('.panel-wrap:hover').length + "," + $('#img-sandwich:hover').length);
        $('#clicker').prop('checked', false);
    } else {
        //console.log("panel not opened: " + $( '#clicker' ).prop('checked') + "," + $('.panel-wrap:hover').length + "," + $('#img-sandwich:hover').length);
    }
});

var canvasWidth = ($(window).width() - 550 - 100) / 2;
if ((canvasWidth + 100) >= 612) {
    canvasWidth = 512;
}

/* window.fetch(URL)
	      .then(response => response.arrayBuffer())
		  .then(arrayBuffer => context.decodeAudioData(arrayBuffer))
		  .then(audioBuffer => {
		      console.log( URL + ' is loaded!');
			  $('#player').prop('disabled', false);
			  $('#loader').css('display','none');
			  $('.player-loader').css('display','none');
			  $('.player-icon').css('display','inline-block');
			  playBuffer = audioBuffer;
	        });
      */

var waveformSvg = document.getElementById('waveform');

waveformSvg.addEventListener('click', function(e) {
    var audio = document.getElementById('audio-waveform');
    var end = $('#waveform').attr('width') - 40;
    var margin = 20;
    var offset = this.getClientRects()[0];
    var res = e.clientX - offset.left - margin;
    if (res < 0) {
        res = 0;
    }
    var newstart = Math.floor(audio.duration * res / end);
    if (newstart >= audio.duration) {
        newstart = Math.floor(audio.duration);
    }
    PlayerStop();
    PlayerPlay(newstart);
    $('#player').prop('checked', true);
    console.log('player started from ' + newstart + ' sec');
}, false);

function isBuffered() {
    var finalize_true = setTimeout(
        function() {
            if ((typeof source === 'undefined')) {
                var audio = document.getElementById('audio-waveform');
                source = context.createMediaElementSource(audio);
                source.connect(splitter);
                source.connect(context.destination);
                console.log(audio.src + ' is loaded!');
                $('#player').prop('disabled', false);
                $('#loader').css('display', 'none');
                $('.player-loader').css('display', 'none');
                $('.player-icon').css('display', 'inline-block');
            }
        },
        3000
    );

};
//playButton.onchange = () => play(yodelBuffer);

function PlayerPlay(sec) {
    var status = $('#player').prop('disabled');
    if (status) {
        return;
    }
    playAudioBuffer(sec);
    /*
		  clearTimeout(ZmeikaTimer);
          zmeika_stop   = 0;
          hideAllPolys();
          zmeika(randomInt2(delta,max_s), randomInt(0,1));
		  */
    var data_step = 0;
    var seconds = 0;
    var minutes = 0;
    var progress = 0;
    if (sec > 0) {
        seconds = sec;
    }
    console.log(seconds);
    //var end = screen.width - 40;
    var end = $('#waveform').attr('width') - 40;
    var koeff = end / d_filtered_1.length;
    var progress_step = end / (3954 * koeff);
    downloadTimer = setInterval(function() {
        seconds++;
        minutes = parseInt(seconds / 60);
        //if ( minutes >= 60) { minutes = 0; }
        //progress += progress_step;
        progress = progress_step * seconds;
        if ((progress * koeff) > end) {
            PlayerStop();
            $('#player').prop('checked', false);
            clearInterval(downloadTimer);
        } else {
            $('.div-waveform-bg').css('background-size', progress * koeff + 'px 100%');
            $('#waveform').css('background-position', ((progress * koeff) + 19) + 'px 19px');
            $('.div-container-time').text(pad(parseInt(minutes / 60)) + ':' + pad(minutes % 60) + ':' + pad(seconds % 60));
            data_step = Math.floor(d_filtered_1['length'] / end);
            var d1 = d_filtered_1.slice(0, progress);
            var d2 = d_filtered_2.slice(0, progress);
            var d3 = d_filtered_3.slice(0, progress);
            var d4 = d_filtered_4.slice(0, progress);
            var d5 = d_filtered_5.slice(0, progress);
            chart.curvedLine(d1, "#c200ff", 0.7, 'data_1_play');
            chart.curvedLine(d2, 'lime', 0.7, 'data_2_play');
            chart.curvedLine(d3, 'white', 0.7, 'data_3_play');
            chart.curvedLine(d4, 'red', 0.7, 'data_4_play');
            chart.curvedLine(d5, 'blue', 0.7, 'data_5_play');
            var inx = Math.floor(progress);
            /*if( !(typeof d_filtered_1[inx] === 'undefined') && !(typeof d_filtered_2[inx] === 'undefined') && !(typeof d_filtered_3[inx] === 'undefined') && !(typeof d_filtered_4[inx] === 'undefined') && !(typeof d_filtered_5[inx] === 'undefined') ) {
                console.log( progress.toFixed(3) + '> β: ' + d_filtered_1[inx]['x'].toFixed(2) +','+d_filtered_1[inx]['y'].toFixed(2) + '; ' + 'α: ' + d_filtered_2[inx]['x'].toFixed(2) +','+d_filtered_2[inx]['y'].toFixed(2) + '; ' + 'θ1: ' + d_filtered_3[inx]['x'].toFixed(2) +','+d_filtered_3[inx]['y'].toFixed(2) + '; ' + 'θ2: ' + d_filtered_4[inx]['x'].toFixed(2) +','+d_filtered_4[inx]['y'].toFixed(2) + '; ' + 'δ: ' + d_filtered_5[inx]['x'].toFixed(2) +','+d_filtered_5[inx]['y'].toFixed(2) + '; ' );
            }*/
            /*
            1 - beta
            2 - alpha
            3 - delta
            4 - high theta
            5 - low theta
            */
            var alpha = d_filtered_2[inx]['y'];
            var beta = d_filtered_1[inx]['y'];
            var htheta = d_filtered_4[inx]['y'];
            var ltheta = d_filtered_5[inx]['y'];
            var delta = d_filtered_3[inx]['y'];
            brainWave(seconds, alpha, beta, htheta, ltheta, delta);
        }
    }, 1000);
}

function PlayerStop() {
    stopAudioBuffer();
    d3.selectAll('.data_1_play').remove();
    d3.selectAll('.data_2_play').remove();
    d3.selectAll('.data_3_play').remove();
    d3.selectAll('.data_4_play').remove();
    d3.selectAll('.data_5_play').remove();
    clearInterval(downloadTimer);
    $('.div-waveform-bg').css('background-size', '0px 100%');
    $('#waveform').css('background-position', '20px 19px');
    $('.div-container-time').text(pad(0) + ':' + pad(0));
    resetOpacity();
}

function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    } else {
        return valString;
    }
}

function PlayerInit() {
    var status = $('#player').prop('checked');
    if (!status) {
        PlayerPlay(0);
        console.log('player: start playing');
    } else {
        PlayerStop();
        console.log('player: stop playing');
    }
}

function ToggleAnimations() {
    var status = $('#toggleanimate').prop('checked');
    if (status) {
        //clearTimeout(ZmeikaTimer);
        //zmeika_stop = 1;
        $('.svg-canvas').hide();
        $('.svg-map').hide();
        $('#analyzer').hide();
    } else {
        //clearTimeout(ZmeikaTimer);
        //zmeika_stop = 0;
        //hideAllPolys();
        //zmeika(randomInt2(delta,max_s), randomInt(0,1));
        $('.svg-canvas').show();
        $('.svg-map').css("display", "inline-block");
        if ($('#player').prop('checked')) {
            $('#analyzer').show();
        }
        //console.log('animations: enable');
    }
}

function ToggleOscillator() {
    var status = $('#toggleoscillo').prop('checked');
    if (status) {
        $('.div-container-scenatio').hide();
        $('.div-waveform-legend').hide();
        //console.log('oscillator: hidden');
    } else {
        $('.div-container-scenatio').show();
        $('.div-waveform-legend').show();
        //console.log('oscillator: visible');
    }
}

function ToggleMeditation() {
    var status = $('#togglemeditation').prop('checked');
    if (status) {
        //clearTimeout(ZmeikaTimer);
        //zmeika_stop = 1;
        $('.div-container-time').css("position", "absolute");
        $('.div-container-time').css("top", "20px");
        $('.div-container-time').css("left", "20px");
        $('.div-container-time').css("opacity", ".4");
        $('#clicker').prop('checked', false);
        $('#toggleoscillo').prop('checked', false);
        $('#toggleoscillo').prop('disabled', true);
        $('#toggleanimate').prop('checked', false);
        $('#toggleanimate').prop('disabled', true);
        /*
			  $('.svg-canvas').addClass('hidden-element');
              $('.svg-map').addClass('hidden-element');
              $('#analyzer').addClass('hidden-element');
              $('.div-container-scenatio').addClass('hidden-element');
			  $('.div-waveform-legend').addClass('hidden-element');
			  */
        $('.svg-canvas').hide();
        $('.svg-map').css("display", "none");
        $('#analyzer').hide();
        $('.div-container-scenatio').hide();
        $('.div-waveform-legend').hide();
        $('#body').addClass('stars');
        $('.twinkling').removeClass('hidden-element');
        $('.emuse-logo').addClass('hidden-element');
        //console.log('meditation mode: on');
    } else {
        //$( '#clicker' ).prop('checked', false);
        //clearTimeout(ZmeikaTimer);
        //zmeika_stop = 0;
        $('.div-container-time').css("position", "relative");
        $('.div-container-time').css("top", "auto");
        $('.div-container-time').css("left", "auto");
        $('.div-container-time').css("opacity", "1");
        $('.div-container-scenatio').show();
        $('.div-waveform-legend').show();
        $('.svg-canvas').show();
        $('.svg-map').css("display", "inline-block");
        if ($('#player').prop('checked')) {
            $('#analyzer').show();
        }
        $('#toggleoscillo').prop('disabled', false);
        $('#toggleanimate').prop('disabled', false);
        //hideAllPolys();
        //zmeika(randomInt2(delta,max_s), randomInt(0,1));
        $('#body').removeClass('stars');
        $('.twinkling').addClass('hidden-element');
        $('.emuse-logo').removeClass('hidden-element');
        //console.log('meditation mode: off');
    }
}

function playAudioBuffer(sec) {
    var audio = document.getElementById('audio-waveform');
    audio.currentTime = sec;
    audio.volume = 0.5;
    var status = $('#togglemeditation').prop('checked');
    if (!status) {
        $('#analyzer').removeClass('hidden-element');
    }
    context.resume().then(() => {
        console.log('Audio context resumed successfully');
    });
    //source = context.createBufferSource();
    //source.buffer = playBuffer;
    //merger.connect(context.destination);
    //source.start();
    audio.play();
}

function stopAudioBuffer() {
    var audio = document.getElementById('audio-waveform');
    $('#analyzer').removeClass('hidden-element');
    $('#analyzer').addClass('hidden-element');
    //source.stop();
    audio.pause();
    //audio.currentTime = 0;
}

const waveform_l = new Float32Array(analyser_l.frequencyBinCount);
const waveform_r = new Float32Array(analyser_r.frequencyBinCount);

analyser_l.getFloatTimeDomainData(waveform_l);
analyser_r.getFloatTimeDomainData(waveform_r);

function updateWaveform() {
    requestAnimationFrame(updateWaveform);
    analyser_l.getFloatTimeDomainData(waveform_l);
    analyser_r.getFloatTimeDomainData(waveform_r);
}

updateWaveform();

//console.log(canvasWidth + '_' + canvasHeight);
oscCanvas_l.width = canvasWidth;
oscCanvas_l.height = canvasHeight;
oscCanvas_r.width = canvasWidth;
oscCanvas_r.height = canvasHeight;


const canvasContext_l = oscCanvas_l.getContext("2d")
const canvasContext_r = oscCanvas_r.getContext("2d");

canvasContext_l.shadowColor = 'lime';
canvasContext_l.shadowBlur = 10;
canvasContext_l.shadowOffsetX = -10;
canvasContext_r.shadowColor = 'lime';
canvasContext_r.shadowBlur = 10;
canvasContext_r.shadowOffsetX = 10;

function drawOscilloscope() {
    var amp_koeff = 2;
    requestAnimationFrame(drawOscilloscope);
    canvasContext_l.clearRect(0, 0, waveform_l.length, canvasHeight);
    canvasContext_l.beginPath();
    canvasContext_l.strokeStyle = "white";
    canvasContext_l.lineWidth = 2;
    for (let i = 0; i < waveform_l.length; i++) {
        const x = i;
        const y = (0.5 + waveform_l[i] / amp_koeff) * canvasHeight;
        if (i === 0) {
            canvasContext_l.moveTo(x, y);
        } else {
            canvasContext_l.lineTo(x, y);
        }
    }
    canvasContext_l.stroke();
    canvasContext_r.clearRect(0, 0, waveform_r.length, canvasHeight);
    canvasContext_r.beginPath();
    canvasContext_r.strokeStyle = "white";
    canvasContext_r.lineWidth = 2;
    for (let i = waveform_r.length; i >= 0; i--) {
        const x = i;
        const y = (0.5 + waveform_r[i] / amp_koeff) * canvasHeight;
        if (i === 0) {
            canvasContext_r.moveTo(x, y);
        } else {
            canvasContext_r.lineTo(x, y);
        }
    }
    canvasContext_r.stroke();
}
drawOscilloscope();
